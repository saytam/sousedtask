import React from 'react';



const generateInput = (props) => {
    return props.inputs.map((name) => {
        return <div onClick={() => props.clicked(name)} key={name} style={{textTransform: 'capitalize'}} className='dropdown-item'>{name}</div>
    })
}

export default generateInput;