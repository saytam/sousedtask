import React from 'react';

import '../../GlobalStyles/styles.css';

const displayInput = props => {
  let inputStyle = props.inputType + 'Fam';
  if (props.inputType !== 'none') {
    return (
      <div className='inputWrapper'>
        <div className='numbersInLine'></div>
        <input name={props.title} type={props.inputType} className={inputStyle}/>
      </div>
    );
  } else {
    return <div />;
  }
};

export default displayInput;