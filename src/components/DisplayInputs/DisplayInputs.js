import React from 'react';

import '../DisplayInputs/DisplayInputs';
import '../../GlobalStyles/styles.css';

const displayInputs = props => {
return props.savedInputs.map( (oneInput, index) => {
    let inputStyle = oneInput + 'Fam';

    if (oneInput !== 'none') {
    return (
    <div key={index} className='inputWrapper'>
    <div className='numbersInLine'>{'#' + ++index}</div>
    <input name={oneInput} type={oneInput} className={inputStyle}/>
     </div>
    )
} else {
    return <div />
}
})
}

export default displayInputs;